#!/bin/bash


#clone the branch
if [ ! -e /tmp/code-deploy-repo ]
then
    echo "cloning branch"
    ssh-agent bash -c 'ssh-add ~/repo-priv-key.pem; git clone $GIT_REPO /tmp/code-deploy-repo';
fi

#get the latest
echo "Pulling the latest $GIT_BRANCH code";
cd /tmp/code-deploy-repo;
ssh-agent bash -c 'ssh-add ~/repo-priv-key.pem; git fetch; git checkout origin/$GIT_BRANCH; git pull origin $GIT_BRANCH';



#define the bucket file
bucketfile=$GIT_BRANCH-$(date -d "today" +"%Y%m%d%H%M").tar.gz;


#push the code
echo "Pushing the deployment to the s3 bucket"
aws deploy push --application-name $ENVIRONMENT --description "This is a revision for the application $ENVIRONMENT" --s3-location s3://$ARTIFACT_BUCKET/$bucketfile --source /tmp/code-deploy-repo 

#do deployment
echo "Running deployment FrontEnd"
aws deploy create-deployment --application-name $ENVIRONMENT --s3-location bucket=$ARTIFACT_BUCKET,key=$bucketfile,bundleType=zip --deployment-group-name $ENVIRONMENT-frontend --description "Deployment for $GIT_BRANCH on $ENVIRONMENT"


echo "Running deployment Admin"
aws deploy create-deployment --application-name $ENVIRONMENT --s3-location bucket=$ARTIFACT_BUCKET,key=$bucketfile,bundleType=zip --deployment-group-name $ENVIRONMENT-admin --description "Deployment for $GIT_BRANCH on $ENVIRONMENT"





