#!/bin/bash

#check for aws key
if [ ! -e ~/.aws/credentials ]
then
    echo "Enter your AWS Access Key";
    read AWS_ACCESS_KEY

    echo "Enter your AWS Secret Key";
    read AWS_SECRET_ACCESS_KEY;

    echo "Enter your AWS Default Region";
    read AWS_DEFAULT_REGION


    #write the config file
    mkdir ~/.aws;
    echo "[default]" >> ~/.aws/credentials;
    echo "aws_access_key_id=$AWS_ACCESS_KEY" >> ~/.aws/credentials;
    echo "aws_secret_access_key=$AWS_SECRET_ACCESS_KEY" >> ~/.aws/credentials;

    echo "[default]" >> ~/.aws/config;
    echo "region = $AWS_DEFAULT_REGION" >> ~/.aws/config;
fi

#check for ssh key
if [ ! -e ~/privkey.pem ]
then
    echo "Please copy and paste the SSH private key for the repository $PRIVKEY";
    while read line
    do
        # break if the line is empty
        [ -z "$line" ] && break
         echo "$line" >> ~/privkey.pem
    done


    #write the key to file
    chmod 600 ~/privkey.pem;
    echo "Saved!";
fi


#check for git key
if [ ! -e ~/repo-priv-key.pem ]
then
    echo "Please copy and paste the git repo private key for the repository $GIT_REPO";
    while read line
    do
        # break if the line is empty
        [ -z "$line" ] && break
         echo "$line" >> "~/repo-priv-key.pem"
    done


    #write the key to file
    chmod 600 ~/repo-priv-key.pem;
    echo "Saved!";
fi




sudo yum install -y git;
sudo yum install -y pv;


sudo yum install -y gcc;
cd /etc && sudo wget http://download.redis.io/redis-stable.tar.gz && sudo tar xvzf /etc/redis-stable.tar.gz && cd /etc/redis-stable && sudo make;
sudo cp /etc/redis-stable/src/redis-cli /usr/bin/;
